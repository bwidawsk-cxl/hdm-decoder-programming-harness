SRC = $(wildcard *.c)
OBJ = $(SRC:.c=.o)
CFLAGS = -Wall -ggdb -O0 `pkg-config --cflags glib-2.0 --cflags json-glib-1.0`
LDFLAGS = `pkg-config --libs glib-2.0 --libs json-glib-1.0`

%.o: hdm.h

hdm: $(OBJ)
	$(CC) $^ $(CFLAGS) $(LDFLAGS) -o $@

.PHONY: clean
clean:
	rm -f $(OBJ) hdm tags TAGS
