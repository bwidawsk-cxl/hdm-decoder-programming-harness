#include <assert.h>
#include <stdio.h>
#include <stdbool.h>
#include <glib-object.h>
#include <json-glib/json-glib.h>

#include "hdm.h"

static struct cxl_decoder *pop_decoder(struct cxl_component *c)
{
	if (!c->decoder) {
		c->decoder = calloc(1, sizeof(*c->decoder));
		c->decoder->parent = c;
		for (int i = 0; i < 8; i++)
			c->decoder->target_list[i] = -1;
	}

	return c->decoder;
}

#define get_topology_lock()
#define put_topology_lock()
#define push_decoder(c)

static int region_ways(struct cxl_region *region)
{
	return region->iw;
}

static inline struct cxl_component *to_cxl_component(struct cxl_endpoint *endpoint)
{
	struct cxl_component *c;

	c = from_id(endpoint->id);
	assert(c != NULL);
	assert(c->type == CXL_ENDPOINT);

	return c;
}

static inline struct hostbridge *to_hostbridge(struct cxl_component *component)
{
	struct hostbridge *hb;
	foreach_hostbridge(hb) {
		if (hb->id == to_id(component))
			return hb;
	}

	return NULL;
}

static inline struct cxl_endpoint *to_endpoint(struct cxl_region *region,struct cxl_component *component)
{
	struct cxl_endpoint *e;

	foreach_endpoint(e, region)
		if (e->id == to_id(component))
			return e;

	return NULL;
}

/* Does the CFMWS entry have the hostbridge ordered specified by hbs */
static inline bool cfmws_ordered(struct cfmws_entry *cfmws, struct hostbridge *hbs[])
{
	return true;
}

static int get_child_order(struct cxl_component *child)
{
	struct cxl_component *c, *parent = from_id(child->parent_id);
	int i = 0;

	foreach_child(c, parent) {
		if (child == c)
			return i;
		i++;
	}

	return -1;
}


/* Is the CXL component a host bridge */
static bool is_hostbridge(struct cxl_component *component)
{
	return component->type == CXL_HOSTBRIDGE;
}

/* Walk topology from top down (DFS) starting at component @c */
static void __top_down(struct cxl_region *r, struct cxl_component *c, void *arg, void(*cb)(struct cxl_region *region, struct cxl_component *c, void *))
{
	struct cxl_component *child;

	cb(r, c, arg);

	foreach_child(child, c) {
		__top_down(r, child, arg, cb);
	}
}

/* Walk topology down (DFS) from the base platform */
static void top_down(struct platform *platform, struct cxl_region *r, void *arg, void(*cb)(struct cxl_region *region, struct cxl_component *c, void *))
{
	struct hostbridge *hb;

	foreach_hostbridge(hb) {
		struct cxl_component *hb_comp = from_id(hb->id);
		struct cxl_component *child;

		cb(r, hb_comp, arg);

		foreach_child(child, hb_comp) {
			__top_down(r, child, arg, cb);
		}
	}
}

/* Get a component's parent - next highest component in the topology */
static struct cxl_component *get_parent(struct cxl_component *child)
{
	if (child->type == CXL_HOSTBRIDGE)
		return NULL;

	return from_id(child->parent_id);
}

/* Get the hostbridge for a given endpoint */
static struct hostbridge *get_hostbridge(struct cxl_region *region, int endpoint)
{
	struct cxl_endpoint *ep = region->endpoints[endpoint];
	struct cxl_component *c = from_id(ep->id);

	while (!is_hostbridge(c))
		c = get_platform()->components[c->parent_id];

	return to_hostbridge(c);
}

/* Get a list of unique hostbridges for this region */
static int get_unique_hostbridges(struct cxl_region *region, struct hostbridge **hbs)
{
	int hb_count = 0;

	if (hbs == NULL)
		hbs = calloc(8, sizeof(*hbs));

	/* Walk down each hostbridge to get the ordered, unique hostbridges */
	for (int i = 0; i < region_ways(region); i++) {
		struct hostbridge *hb = get_hostbridge(region, i);
		bool found = false;

		/* Uniquely add the hostbridges */
		for (int j = 0; j < hb_count; j++) {
			if (hbs[j] == hb)
				found = true;
		}
		if (!found)
			hbs[hb_count++] = hb;
	}

	return hb_count;
}

/* Get the depth of the topology for the given endpoint */
static int endpoint_depth(struct cxl_endpoint *endpoint)
{
	struct cxl_component *c;
	int i = 0;

	for (c = to_cxl_component(endpoint); !is_hostbridge(c) && (c = from_id(c->parent_id)); ++i);

	assert(i > 0);

	return i;
}

/* Returns true if the CXL component is comrised within the region */
static bool in_region(struct cxl_region *region, struct cxl_component *c)
{
	struct cxl_component *child;
	bool ret = false;

	if (c->type == CXL_ENDPOINT) {
		for (int i = 0; i < region_ways(region); i++)
			if (to_cxl_component(region->endpoints[i]) == c)
				return true;
	}

	foreach_child(child, c) {
		ret = ret || in_region(region, child);
	}

	return ret;
}

/* Get the number of siblings, not including self, for this component @c */
static int get_sibling_count(struct cxl_region *region, struct cxl_component *c)
{
	struct cxl_component *child;
	int count = 0;

	/* HOSTBRIDGE HACK */
	if (c->type == CXL_HOSTBRIDGE)
		return get_unique_hostbridges(region, NULL) - 1;

	foreach_child(child, get_parent(c)) {
		if (child == c)
			continue;
		count += in_region(region, child) ? 1 : 0;
	}

	return count;
}

/**
 * is_region_perfectly_balanced() - Are all leaves the same depth
 */
static bool is_region_perfectly_balanced(struct cxl_region *region)
{
	struct cxl_endpoint *ep;
	int depth = 0;

	// Check same depth
	foreach_endpoint(ep, region) {
		if (!depth)
			depth = endpoint_depth(ep);
		else
			if (depth != endpoint_depth(ep))
				return false;
	}

	return true;
}

/*
 * Find an appropriate CFMWS entry for the given region
 */
static struct cfmws_entry *find_cfmws(struct cxl_region *region)
{
	struct hostbridge *hbs[8] = { NULL} ; // at most iw hostbridges are possible
	struct cfmws_entry *cfmws;
	int hb_count;

	hb_count = get_unique_hostbridges(region, hbs);

	foreach_cfmws_entry(cfmws) {
		if (cfmws->ways != hb_count)
			continue;

		if (cfmws_ordered(cfmws, hbs))
			return cfmws;
	}

	return NULL;
}

static struct cxl_endpoint *find_lowest_endpoint(struct cxl_region *region, struct cxl_component *c, int low_bar)
{
	struct cxl_component *child;
	struct cxl_endpoint *ret = NULL;

	if (c->type == CXL_ENDPOINT) {
		struct cxl_endpoint *ep = to_endpoint(region, c);

		if (ep->order >= low_bar)
			return ep;
	}

	foreach_child_in_region(child, region, c) {
		struct cxl_endpoint *temp = find_lowest_endpoint(region, child, low_bar);
		if (!temp)
			continue;

		if (!ret || ret->order > temp->order)
			ret = temp;
	}

	return ret;
}

static void setup_interleave(struct cxl_region *region, struct cxl_component *c, struct cxl_component *children[], int num_children)
{
	int low = 0;

	for (int j = 0; j < num_children; j++) {
		struct cxl_component *temp;
		struct cxl_endpoint *ep = find_lowest_endpoint(region, c, low);
		bool found = false;

		if (!ep)
			continue;

		temp = to_cxl_component(ep);

again:
		for (int k = 0; k < num_children && !found; k++) {
			if (temp == children[k]) {
				struct cxl_decoder *d = pop_decoder(c);
				d->iw = get_sibling_count(region, c) + 1;
				d->target_list[d->ndx++] = get_child_order(temp);
				found = true;
			}
		}
		if (!found) {
			temp = get_parent(temp);
			goto again;
		}

		low = ep->order + 1;
	}
}


/* Callback to sanity check and configure granularity */
static void topologize(struct cxl_region *region, struct cxl_component *c, void *arg)
{
	struct cxl_decoder *decoder = pop_decoder(c);
	struct cxl_decoder *parent_decoder;
	const int siblings = get_sibling_count(region, c) + 1;
	int ig = *(int *)arg;
	struct cxl_component *child, *children[8] = { };
	bool bottom = false;
	int i = 0;

	if (is_hostbridge(c))
		decoder->ig = ig + ((siblings > 1) ? 1 : 0);
	else if (c->type == CXL_ENDPOINT)
		decoder->ig = region->ig;
	else {
		parent_decoder = pop_decoder(from_id(c->parent_id));
		decoder->ig = parent_decoder->ig + ((siblings > 1) ? 1 : 0);
	}

	/* MOVEME somewhere */
	/* Check that the region device interleave is correct */
	foreach_child_in_region(child, region, c) {
		if (child->type == CXL_ENDPOINT) {
			bottom = true;
		}
		children[i++] = child;
	}

	setup_interleave(region, c, children, i);

	if (!bottom)
		return;

	for (--i; i >= 1; i--) {
		struct cxl_endpoint *sibling_a, *sibling_b;
		sibling_a = to_endpoint(region, children[i]);
		sibling_b = to_endpoint(region, children[i - 1]);

		int delta = abs(sibling_a->order - sibling_b->order);

		if (delta != region->iw / siblings) {
			fprintf(stderr, "Bad region device ordering\n");
			abort();
		}
	}
}

static bool __program_hdm_decoders(struct cxl_region *region)
{
	struct cfmws_entry *cfmws;

	if (!(cfmws = find_cfmws(region))) {
		fprintf(stderr, "No CEDT for region\n");
		return false;
	}

	printf("CFWMS: x%d, %dIG, %zub\n", cfmws->ways, cfmws->granularity, cfmws->size);

	int ig = region->ig;
	top_down(get_platform(), region, &ig, topologize);

	return true;
}

static void __print_decoder(struct cxl_decoder *decoder)
{
	printf("Component %d ", to_id(decoder->parent));
	switch (decoder->parent->type) {
	case CXL_HOSTBRIDGE:
		printf("(hostbr) ");
		break;
	case CXL_SWITCH:
		printf("(switch) ");
		break;
	case CXL_ENDPOINT:
		printf("(endpoint)\n");
		return;
	default:
		abort();
	}

	printf("\t\tIW: %d\tIG: %d", decoder->iw, decoder->ig);
	printf("\t[ ");
	for (int i = 0; i < decoder->iw; i++) {
		printf("%d, ", decoder->target_list[i]);
	}
	printf("]");

	printf("\n");
}

static void print_decoder(struct cxl_region *region, struct cxl_component *c, void *arg)
{
	if (c->decoder)
		__print_decoder(c->decoder);
}

static bool program_hdm_decoders(struct cxl_region *region)
{
	get_topology_lock();

	if (!is_region_perfectly_balanced(region)) {
		fprintf(stderr, "Topology isn't balanced\n");
		return false;
	}

	if (!__program_hdm_decoders(region)) {
		fprintf(stderr, "programming failed\n");
		return false;
	}

	top_down(get_platform(), region, NULL, print_decoder);

	put_topology_lock();

	return true;
}

int main(int argc, char *argv[])
{
	create_platform(argv[1]);
	struct cxl_region *region = create_region(argv[2]);
	bool p;

	p = program_hdm_decoders(region);
	printf("programming is %spossible\n", p ? "" : "im");

	return 0;
}
