#include <assert.h>
#include <stdbool.h>
#include <stdlib.h>
#include <glib-object.h>
#include <json-glib/json-glib.h>

#ifndef _HDM_H_
#define _HDM_H_

#ifdef DEBUG
#define DEBUG_PRINT(...) do{ fprintf( stderr, __VA_ARGS__ ); } while( false )
#else
#define DEBUG_PRINT(...) do{ } while ( false )
#endif

#define MAX_COMPONENTS 256
#define MAX_DOWNSTREAM_PORTS 8
#define MAX_INTERLEAVE 8
#define MAX_HOSTBRIDGES 8

enum component_type {
	CXL_INVALID,
	CXL_SWITCH,
	CXL_ENDPOINT,
	CXL_HOSTBRIDGE,
};

struct cxl_component {
	enum component_type type;

	int parent_id;
	int connections[MAX_DOWNSTREAM_PORTS];
	int connected_ports;

	struct cxl_decoder *decoder;
};

struct global_decoder_info {
	int decoder_count;
	int target_count;
	/* TODO: Assume all interleave granularity is supported */
};

struct cxl_decoder {
	struct global_decoder_info *global;
	struct cxl_component *parent;

	struct cxl_endpoint *ep[MAX_DOWNSTREAM_PORTS];
	int ndx;

	struct {
		unsigned long long base;
		unsigned long long size;
		int ig;
		int iw;
		int target_list[MAX_DOWNSTREAM_PORTS];
	};
};

struct cxl_endpoint {
	size_t capacity;
	struct cxl_decoder *hdm_decoders[MAX_DOWNSTREAM_PORTS];

	int id;
	int order; // this endpoints position in the region
};

struct cxl_region {
	struct cxl_endpoint *endpoints[MAX_INTERLEAVE];
	size_t size;
	unsigned ig;
	unsigned iw;
};

struct hostbridge {
	int id;
	int num_decoders;
	int connections[MAX_DOWNSTREAM_PORTS];
};

struct cfmws_entry {
	unsigned ways;
	unsigned granularity;
	size_t size;
};

struct cedt {
	struct cfmws_entry entries[4];
};

struct platform {
	struct cedt cedt;
	struct hostbridge hostbridges[MAX_HOSTBRIDGES];
	struct cxl_component *components[MAX_COMPONENTS];
};

#define foreach_cfmws_entry(cfmws) \
	for (cfmws = &get_platform()->cedt.entries[0]; (void *)cfmws < (void *)&get_platform()->hostbridges[0]; cfmws++)

#define foreach_hostbridge(hb) \
	for (hb = &get_platform()->hostbridges[0]; hb->num_decoders > 0; hb++)

#define __foreach_child(i, comp) \
	for (int j = 0, i = comp->connections[j]; i > 0; i = comp->connections[++j])

#define foreach_child(child, parent) \
	for (int j = 0; parent->connections[j] > 0 && (child = from_id(parent->connections[j])); j++)

#define foreach_child_in_region(child, region, parent) \
	for (int j = 0; parent->connections[j] > 0 && (child = from_id(parent->connections[j])) && in_region(region, child); j++)

#define foreach_endpoint(ep, region) \
	for (struct cxl_endpoint **__t = region->endpoints; (void *)__t < (void *)region->endpoints + sizeof(region->endpoints) && ((ep) = *__t); __t++)

void create_platform(const char *json);
struct cxl_region *create_region(const char *json);
struct platform *get_platform(void);

static inline int to_id(struct cxl_component *component)
{
	for (int i = 0; i < MAX_COMPONENTS; i++)
		if (component == get_platform()->components[i])
			return i;

	return -1;
}

static inline struct cxl_component *from_id(int id)
{
	return get_platform()->components[id];
}

static inline int json_read_int(JsonReader *reader, const char *key)
{
	int ret;

	assert(json_reader_read_member(reader, key));
	ret = json_reader_get_int_value(reader);
	json_reader_end_member(reader);

	return ret;
}

static inline double json_read_double(JsonReader *reader, const char *key)
{
	double ret;

	assert(json_reader_read_member(reader, key));
	ret = json_reader_get_double_value(reader);
	json_reader_end_member(reader);

	return ret;
}

static inline const gchar *json_read_string(JsonReader *reader, const char *key)
{
	assert(json_reader_read_member(reader, key));
	const char *ret = json_reader_get_string_value(reader);
	json_reader_end_member(reader);

	return ret;
}

#endif
