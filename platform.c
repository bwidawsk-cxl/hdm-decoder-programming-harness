#include <assert.h>
#include <stdio.h>
#include <stdbool.h>
#include <glib-object.h>
#include <json-glib/json-glib.h>

#include "hdm.h"

// Endpoint LUT
struct cxl_endpoint endpoints[MAX_COMPONENTS];

struct platform plat;

static void parse_cedt(JsonReader *reader)
{
	int cfmws_entries, i;

	assert(json_reader_read_member(reader, "CEDT"));
	assert(json_reader_read_member(reader, "cfmws"));

	cfmws_entries = json_reader_count_elements(reader);

	DEBUG_PRINT("Found %d CFMWS entries\n", cfmws_entries);

	// for each cfmws
	for (i = 0; i < cfmws_entries; i++) {
		json_reader_read_element(reader, i);

		plat.cedt.entries[i].ways = json_read_int(reader, "ways");
		plat.cedt.entries[i].granularity =  json_read_int(reader, "ig");
		plat.cedt.entries[i].size =  json_read_double(reader, "size");

		DEBUG_PRINT("\tx%d +%zx\n", plat.cedt.entries[i].ways, plat.cedt.entries[i].size);

		json_reader_end_element(reader);
	}
	json_reader_end_member(reader); // cfmws
	json_reader_end_member(reader); // CEDT
}

static void parse_hostbridges(JsonReader *reader)
{
	int n, i;

	assert(json_reader_read_member(reader, "Hostbridges"));
	n = json_reader_count_elements(reader);

	DEBUG_PRINT("Found %d hostbridges\n", n);

	for (i = 0; i < n; i++) {
		int id;

		json_reader_read_element(reader, i);

		id = json_read_int(reader, "id");
		plat.components[id] = calloc(1, sizeof(struct cxl_component));
		plat.components[id]->type = CXL_HOSTBRIDGE;
		plat.components[id]->parent_id = -1;
		plat.hostbridges[i].id = id;
		plat.hostbridges[i].num_decoders = json_read_int(reader, "num_decoders");
		assert(json_reader_read_member(reader, "downstream_connect"));
		int connections = json_reader_count_elements(reader);
		for (int j = 0; j < connections; j++) {
			json_reader_read_element(reader, j);
			plat.hostbridges[i].connections[j] = json_reader_get_int_value(reader);
			plat.components[id]->connections[j] = plat.hostbridges[i].connections[j];
			json_reader_end_element(reader);
		}

		json_reader_end_member(reader); // downstream_connect
		plat.components[id]->connected_ports = connections;
		json_reader_end_element(reader);
	}

	json_reader_end_member(reader); // Hostbridges
}

static void parse_component(JsonReader *reader)
{
	int id;

	id = json_read_int(reader, "id");

	if (plat.components[id]) {
		fprintf(stderr, "Duplicate ids (%d) in JSON\n", id);
		exit(1);
	}

	plat.components[id] = calloc(1, sizeof(struct cxl_component));

	if (!strcmp(json_read_string(reader, "type"), "switch")) {
		plat.components[id]->type = CXL_SWITCH;
		assert(json_reader_read_member(reader, "downstream_connect"));
		int connections = json_reader_count_elements(reader);
		for (int j = 0; j < connections; j++) {
			json_reader_read_element(reader, j);
			plat.components[id]->connections[j] = json_reader_get_int_value(reader);
			json_reader_end_element(reader);
		}

		json_reader_end_member(reader); // downstream_connect
		plat.components[id]->connected_ports = connections;
	} else {
		plat.components[id]->type = CXL_ENDPOINT;
		endpoints[id].capacity = json_read_int(reader, "capacity");

		DEBUG_PRINT("Found endpoint with %db capacity\n", endpoints[id].capacity);

		/* FIXME: unhardcode decoder info */
		struct global_decoder_info *info = calloc(1, sizeof(*info));

		info->decoder_count = MAX_DOWNSTREAM_PORTS;
		info->target_count = 0;

		for (int i = 0; i < info->decoder_count; i++) {
			endpoints[id].hdm_decoders[i] = calloc(1, sizeof(struct cxl_decoder));
			endpoints[id].hdm_decoders[i]->global = info;
		}

//		endpoints[id].id = id;
	}

}

static void parse_components(JsonReader *reader)
{
	int n;

	assert(json_reader_read_member(reader, "Components"));
	n = json_reader_count_elements(reader);

	DEBUG_PRINT("Found %d components\n", n);

	for (int i = 0; i < n; i++) {
		json_reader_read_element(reader, i);

		parse_component(reader);

		json_reader_end_element(reader);
	}

	json_reader_end_member(reader); // Components
}

static void set_parent_id(struct cxl_component *child, int id)
{
	assert(id > 0);
	child->parent_id = id;
}

static void descend_switch(struct cxl_component *swtch)
{
	assert(swtch->type == CXL_SWITCH);

	__foreach_child(c, swtch) {
		struct cxl_component *child = plat.components[c];
		set_parent_id(child, to_id(swtch));
		if (child->type == CXL_SWITCH)
			descend_switch(child);
	}
}


/* Instantiates all devices in the platform */
void create_platform(const char *json)
{
	JsonParser *parser;
	JsonReader *reader;
	GError *error;

	parser = json_parser_new();

	error = NULL;
	json_parser_load_from_file(parser, json, &error);
	assert(!error);

	reader = json_reader_new(json_parser_get_root(parser));

	parse_cedt(reader);
	parse_hostbridges(reader);
	parse_components(reader);

	g_object_unref(reader);
	g_object_unref(parser);

	struct hostbridge *hb;
	foreach_hostbridge(hb) {
		for (int i = 0; i < MAX_DOWNSTREAM_PORTS; i++) {
			struct cxl_component *comp;

			int downstream = hb->connections[i];

			if (downstream <= 0)
				continue;

			comp = plat.components[downstream];
			if (comp->type == CXL_SWITCH) {
				descend_switch(comp);
			}
			set_parent_id(comp, to_id(plat.components[hb->id]));
		}
	}
}

struct platform *get_platform(void) {
	return &plat;
}
