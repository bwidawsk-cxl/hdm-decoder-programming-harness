#include <stdio.h>
#include "hdm.h"

struct cxl_region *create_region(const char *json)
{
	struct cxl_region *ret;
	JsonParser *parser;
	GError *error;
	int i;

	parser = json_parser_new();
	error = NULL;
	json_parser_load_from_file(parser, json, &error);
	assert(!error);

	ret = calloc(1, sizeof(*ret));

	JsonReader *reader = json_reader_new(json_parser_get_root(parser));
	ret->ig = json_read_int(reader, "ig");
	ret->size = json_read_int(reader, "size");
	ret->iw = json_read_int(reader, "iw");

	assert(json_reader_read_member(reader, "endpoints"));
	int n = json_reader_count_elements(reader);
	for (i = 0; i < n; i++) {
		json_reader_read_element(reader, i);

		ret->endpoints[i] = calloc(1, sizeof(struct cxl_endpoint));
		ret->endpoints[i]->id = json_read_int(reader, "id");
		ret->endpoints[i]->order = json_read_int(reader, "order");

		DEBUG_PRINT("Found region endpoint %d\n", ret->endpoints[i]->id);

		json_reader_end_element(reader);
	}

	json_reader_end_member(reader); // endpoints

	g_object_unref(reader);
	g_object_unref(parser);

	if (i != ret->iw) {
		fprintf(stderr, "Invalid number of endpoints for %d ways\n", ret->iw);
		return NULL;
	}

	return ret;
}
